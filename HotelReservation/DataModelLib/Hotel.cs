﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel;

namespace DataModelLib
{
    [Serializable]
    public class Hotel: IList<HotelRoom>
    {
        private List<HotelRoom> _hotel;

        public int Count
        {
            get { return _hotel.Count;}
        }

        public bool IsReadOnly
        {
            get { return false; }
        }

        public HotelRoom this[int index]
        {
            get { return _hotel[index]; }

            set { if (_hotel != null) _hotel.Insert(index, value); }
        }

        public HotelRoom this[string numberRoom]
        {
            get { return SearchByNumber(numberRoom); }

            set { if (_hotel != null) _hotel.Insert(IndexOf(SearchByNumber(numberRoom)), value); }
        }

        public Hotel()
        {
            _hotel = new List<HotelRoom>();
        }

        public bool BookRoom(HotelRoom room, User user, DateTime dateStartBooking, DateTime dateEndBooking)
        {
            return room != null && user != null && room.ChangeBooking(user, true, dateStartBooking, dateEndBooking);
        }

        public bool UnBookRoom(HotelRoom room, User user)
        {
            return room != null && user != null && room.ChangeBooking(user, false, DateTime.MinValue, DateTime.MinValue);
        }

        public void UnbookAllFreeRooms()
        {
            foreach (var room in _hotel)
                room.ChangeBooking(null, false, DateTime.MinValue, DateTime.MinValue);
        }

        public void Add(string numberRoom, TypeRooms typeRoom, double price, string description, byte numberPlaces)
        {
            this.Add(new HotelRoom(numberRoom, typeRoom, price, description, numberPlaces));
        }

        public void Add(HotelRoom room)
        {
            if (_hotel != null) _hotel.Add(room);
        }

        public void Insert(int index, HotelRoom room)
        {
            if (_hotel != null) _hotel.Insert(index, room);
        }

        public bool Remove(HotelRoom item)
        { 
            return (item != null) && _hotel.Remove(item); 
        }

        public bool RemoveRoom(string numberRoom)
        {
            var foundRoom = SearchByNumber(numberRoom);

            return (foundRoom != null) && _hotel.Remove(foundRoom); 
        }

        public HotelRoom SearchByNumber(string numberRoom)
        {
            return _hotel.FirstOrDefault(x => string.Equals(x.NumberRoom, numberRoom));
        }

        public List<HotelRoom> SearchByType(TypeRooms typeRoom)
        {
            return _hotel.Where(x => x.TypeRoom == typeRoom).ToList();
        }

        public List<HotelRoom> SearchByBooked(bool booked, DateTime dateStartBooking, DateTime dateEndBooking)
        {
            return _hotel.Where(x => x.IsBooked(dateStartBooking, dateEndBooking) == booked).ToList();
        }

        public List<HotelRoom> SearchByNumberPlaces(byte numberPlaces)
        {
            return _hotel.Where(x => x.NumberPlaces == numberPlaces).ToList();
        }

        public void SortByNumberRoom(ListSortDirection sortDirection)
        {
            _hotel = sortDirection == ListSortDirection.Ascending ? _hotel.OrderBy(x => x.NumberRoom).ToList() : _hotel.OrderByDescending(x => x.NumberRoom).ToList();
        }

        public void SortByType(ListSortDirection sortDirection)
        {
            _hotel = sortDirection == ListSortDirection.Ascending ? _hotel.OrderBy(x => (int)x.TypeRoom).ToList() : _hotel.OrderByDescending(x => (int)x.TypeRoom).ToList();
        }

        public void SortByPrice(ListSortDirection sortDirection)
        {
            _hotel = sortDirection == ListSortDirection.Ascending ? _hotel.OrderBy(x => x.Price).ToList() : _hotel.OrderByDescending(x => x.Price).ToList();
        }

        public void Clear()
        {
            _hotel.Clear();
        }

        public int IndexOf(HotelRoom item)
        {
            return _hotel.IndexOf(item);
        }

        public void RemoveAt(int index)
        {
            _hotel.RemoveAt(index);
        }

        public bool Contains(HotelRoom item)
        {
            return _hotel.Contains(item);
        }

        public void CopyTo(HotelRoom[] array, int index)
        {
            _hotel.CopyTo(array, index);
        }

        public IEnumerator<HotelRoom> GetEnumerator()
        {
            return (IEnumerator<HotelRoom>)_hotel.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return (IEnumerator)_hotel.GetEnumerator();
        }
    }
}
