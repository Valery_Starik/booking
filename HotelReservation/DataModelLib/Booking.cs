﻿using System;

namespace DataModelLib
{
    [Serializable]
    public class Booking
    {
        public User UserBookedRoom { get; set; }
        public DateTime DateStartBooking { get; set; }
        public DateTime DateEndBooking { get; set; }

        public Booking(){}

        public Booking(User user, DateTime dateStartBooking, DateTime dateEndBooking)
        {
            UserBookedRoom = user;
            DateStartBooking = dateStartBooking;
            DateEndBooking = dateEndBooking;
        }
    }
}
