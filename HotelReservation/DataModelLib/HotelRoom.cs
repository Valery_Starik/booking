﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace DataModelLib
{
    [Serializable]
    public class HotelRoom: IEquatable<HotelRoom>, IComparable<HotelRoom>
    {
        private List<Booking> _bookingList = new List<Booking>();

        public string NumberRoom { get; set; }
        public TypeRooms TypeRoom { get; set; }
        public double Price{ get; set; }        
        public string Description { get; set; }
        public byte NumberPlaces { get; set; }
        
        public HotelRoom(string numberRoom, TypeRooms typeRoom, double price, string description, byte numberPlaces)
        {
            NumberRoom = numberRoom;
            TypeRoom = typeRoom;
            Price = price;
            Description = description;
            NumberPlaces = numberPlaces;
        }

        public HotelRoom()
        {
            TypeRoom = TypeRooms.Standart;
            NumberPlaces = 1;
        }

        public bool IsBooked(DateTime dateStart, DateTime dateEnd)
        {
            if (dateStart.CompareTo(DateTime.Now) >= 0 && dateEnd.CompareTo(DateTime.Now.AddDays(1)) >= 0)
	        {
                if (_bookingList.Count == 0) return false;    

                if (_bookingList.Count(x => dateStart.CompareTo(x.DateEndBooking) > 0 || dateEnd.CompareTo(x.DateStartBooking) < 0) == _bookingList.Count) 
                    return false;
	        }
            
            return true;
        }

        public bool ChangeBooking(User user, bool book, DateTime dateStart, DateTime dateEnd)
        {
            if (user != null && book && !IsBooked(dateStart, dateEnd))
            {
                var booking = SearchByUser(user);

                if (booking != null)
                {
                    if (!_bookingList.Remove(booking)) return false;   
                }

                _bookingList.Add(new Booking(user, dateStart, dateEnd));

                return true;
            }

            if (user != null && !book)
            {
                var booking = SearchByUser(user);

                if (booking != null) return _bookingList.Remove(booking);       
            }
            else if (user == null && !book)
            {
                _bookingList.RemoveAll(x => x.DateEndBooking.CompareTo(DateTime.Now) < 0);
            }

            return false;
        }

        public Booking SearchByUser(User user)
        {
            return _bookingList.FirstOrDefault(x => x.UserBookedRoom.Equals(user));
        }

        public override string ToString()
        {
            return (string.Format("Number: {0}. Price: {1}. Type: {2}. Booked: {3}",
                NumberRoom, Price, TypeRoom, IsBooked(DateTime.Now, DateTime.Now.AddDays(1)) ? "Yes" : "No"));
        }

        bool IEquatable<HotelRoom>.Equals(HotelRoom room)
        {
            return string.Equals(NumberRoom, room.NumberRoom);
        }

        public int CompareTo(HotelRoom otherRoom)
        {
            return otherRoom == null ? 1 : string.Compare(NumberRoom, otherRoom.NumberRoom, StringComparison.Ordinal);
        }
    }
}
