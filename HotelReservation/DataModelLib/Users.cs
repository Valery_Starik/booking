﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModelLib
{
    [Serializable]
    public class User : IComparable
    {
        public User() { }
        public User(string fName, string lName, string pNumber, string log, string pas, string mail)
        {
            FirstName = fName;
            LastName = lName;
            PhoneNumber = pNumber;
            Login = log;
            Password = pas;
            Mail = mail;
        }
        private string firstName;
        private string lastName;
        private string phoneNumber;
        private int password;
        private string login;
        private string mail;

        public string Password
        {
            set
            {
                password = value.GetHashCode();
            }
            get
            {
                return Convert.ToString(password);
            }
        }

        public string Login
        {
            set
            {
                login = value;
            }
            get
            {
                return login;
            }
        }

        public string Mail
        {
            set
            {
                mail = value;
            }
            get
            {
                return mail;
            }
        }

        public string FirstName
        {
            get
            {
                return firstName;
            }
            set
            {
                firstName = value;
            }
        }
        public string LastName
        {
            get
            {
                return lastName;
            }
            set
            {
                lastName = value;
            }
        }
        public string PhoneNumber
        {
            get
            {
                return phoneNumber;
            }
            set
            {
                phoneNumber = value;
            }
        }
        public int CompareTo(Object obj)
        {
            User usr = obj as User;
            return this.firstName.CompareTo(usr.firstName);
        }
        //public override string ToString()
        //{
        //    return (String.Format("First name: {0}; Last name: {1}; Phone number: {2}",  FirstName, LastName, PhoneNumber));
        //}
        public override string ToString()
        {
            return (String.Format(FirstName));
        }

    }
    [Serializable]
    public class UserCollection
    {

       List<User> users = new List<User>();
        public Object Addition(string data, List<User> users)
        {
            string[] split = data.Split(new Char[] { ' ' });
            string firstName = split[0];
            string lastName = split[1];
            string phoneNumber = split[2];
            string login = split[3];
            string password = split[4];
            string mail = split[5];

            users.Add(new User(firstName, lastName, phoneNumber, login, password, mail));

            return users;
        }
        
        public int Searching(string login, List<User> users)
        {
            int index = users.IndexOf(users.Find(item => item.Login == login));
            return index;
        }

        
    }
}
