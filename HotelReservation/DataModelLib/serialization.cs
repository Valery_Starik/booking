﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace DataModelLib
{
   public class Serialization
    {
        public static void Save(object Collect, string filename)
        {
            FileStream fs = new FileStream(filename, FileMode.OpenOrCreate, FileAccess.Write);

            BinaryFormatter formatter = new BinaryFormatter();
            formatter.Serialize(fs, Collect);
            fs.Close();
        }


        public static object Load(string filename)
        {
            FileStream fs = new FileStream(filename, FileMode.OpenOrCreate);

            BinaryFormatter formatter = new BinaryFormatter();
            object result = formatter.Deserialize(fs);
            fs.Close();
            return result;
        }

        public static void WriteXMLSerial(List<User> users)
        {
            XmlSerializer x = new XmlSerializer(typeof(List<User>));
            TextWriter writer = new StreamWriter("Users.xml");
            x.Serialize(writer, users);
            writer.Close();
        }

        public static List<User> ReadXMLSerial()
        {
            //XmlSerializer x = new XmlSerializer(typeof(List<User>));

            //FileStream fs = new FileStream("Users.xml", FileMode.Open);
            //List<User> users;
            //users = (List<User>)x.Deserialize(fs);
            //fs.Close();
            List<User> users = new List<User>();
            XmlSerializer bf = null;
            FileStream fs = null;
            if (File.Exists("Users.xml"))
            {
                fs = new FileStream("Users.xml", FileMode.Open, FileAccess.Read);
                bf = new XmlSerializer(typeof(List<User>));
                users = (List<User>)bf.Deserialize(fs);
                fs.Close();
            }
            else
            {
                users = new List<User>();
            }
            
            return users;
        }
        public static void WriteXMLSerial(Hotel OurHotel)
        {
            XmlSerializer x = new XmlSerializer(typeof(Hotel));
            TextWriter writer = new StreamWriter("Hotel.xml");
            x.Serialize(writer, OurHotel);
            writer.Close();
        }

       public static Hotel ReadXMLSerialHotel()
       {
            //XmlSerializer x = new XmlSerializer(typeof (Hotel));

            //FileStream fs = new FileStream("Hotel.xml", FileMode.Open);
            //Hotel OurHotel;
            //OurHotel = (Hotel) x.Deserialize(fs);
            //fs.Close();
            //return OurHotel;
            Hotel OurHotel = new Hotel();
           XmlSerializer bf = null;
            FileStream fs = null;
            if (File.Exists("Hotel.xml"))
                {
                    fs = new FileStream("Hotel.xml", FileMode.Open, FileAccess.Read);
                    bf = new XmlSerializer(typeof(Hotel));
                    OurHotel = (Hotel)bf.Deserialize(fs);
                fs.Close();
            }
            else
            {
                OurHotel = new Hotel();
            }
            return OurHotel;

        }




    }
}
