﻿using System;
using System.Net.Mail;

namespace DataModelLib
{
    public class Mail
    {
        private const string FromAddress = "m2.altexsoftlab@gmail.com";
        private const string FromPassword = "AltexSoftLab_M2";
        private const string GoogleSmtpServer = "smtp.gmail.com";
        private const int GoogleSmtpPort = 587;

        private readonly SmtpClient _mailClient;
        private readonly MailAddress _from;
        private MailAddress _to;

        public Mail()
        {
            _mailClient = new SmtpClient(GoogleSmtpServer, GoogleSmtpPort);

            _from = new MailAddress(FromAddress, "Booking");

            _mailClient.EnableSsl = true;           
            _mailClient.DeliveryMethod = SmtpDeliveryMethod.Network;
            _mailClient.UseDefaultCredentials = false;
            _mailClient.Credentials = new System.Net.NetworkCredential(FromAddress, FromPassword);         
        }

        public void SendMail(string addressTo, string subjectMail, string bodyMail)
        {
            _to = new MailAddress(addressTo);

            using (var message = new MailMessage(_from, _to))
            {
                try
                {
                    message.Subject = subjectMail;
                    message.Body = bodyMail;

                    _mailClient.Send(message);
                }
                catch (Exception e)
                {                   
                    throw new Exception("Mail.Send: " + e.Message);
                }
            }
        }
    }
}
