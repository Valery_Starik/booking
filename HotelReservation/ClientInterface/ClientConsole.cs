﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataModelLib;

namespace ClientInterface
{
    public class ClientConsole
    { 
        
        public static void ConsoleViewHeader()
        {
            Console.SetCursorPosition(0, 10);
            Console.ForegroundColor = ConsoleColor.DarkGreen;
            string[] headerColumns = new string[] {ConsoleResourses.NumberRoom, ConsoleResourses.TypeRoom,
                ConsoleResourses.Price, "Book   |", ConsoleResourses.Discription, "Data start book  |", "Data end book  |","User tobook   |" };
            Console.WriteLine("{0,5} {1,0} {2,0} {3,10} {4,20}\n", headerColumns);
        }

        public static void StartConsol()
        {
            Console.Title = "Booking. Made in AltexSoftLab";
            Console.WindowWidth = 100;
            Console.WindowHeight = 45;
            Console.BackgroundColor = ConsoleColor.Gray;
            Console.Clear();
        }
        public static string[] User()
        {
            Console.Clear();
            string[] LogPas = new string[2];
            
            string a = "▄";
            Console.BackgroundColor = ConsoleColor.DarkCyan;
            Console.ForegroundColor = ConsoleColor.White;
            Console.SetCursorPosition(20, 20);
            Console.WriteLine("▄▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▄");
            Console.SetCursorPosition(20, 21);
            Console.WriteLine("▀                                       ▀");
            Console.SetCursorPosition(20, 21);
            Console.WriteLine("▄ Enter please your login and password: ▄");
            Console.SetCursorPosition(20, 22);
                Console.Write("▄ Login: "+ "{0,32}\n", a);
            Console.SetCursorPosition(20, 23);
            Console.SetCursorPosition(20, 23);
            Console.Write("▄ Password: " + "{0,29}\n", a);
            Console.SetCursorPosition(20, 24);
            Console.WriteLine("▀▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▀");
            Console.ForegroundColor = ConsoleColor.Black;
            Console.SetCursorPosition(29, 22);
             LogPas[0] = Console.ReadLine();
            Console.SetCursorPosition(31, 23);
            LogPas[1] = Console.ReadLine();

            Console.BackgroundColor = ConsoleColor.Gray;
            Console.ForegroundColor = ConsoleColor.White;

            Console.Clear();
            return LogPas;
        }
        public static string[] CreateUser()
        {
            Console.Clear();
            string[] LogPas = new string[7];

            string a = "▄";
            Console.BackgroundColor = ConsoleColor.DarkCyan;
            Console.ForegroundColor = ConsoleColor.White;
            Console.SetCursorPosition(20, 20);
            Console.WriteLine("▄▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▄");
            Console.SetCursorPosition(20, 21);
            Console.WriteLine("▀                                       ▀");
            Console.SetCursorPosition(20, 21);
            Console.WriteLine("▄               REGISTRATION            ▄");
            Console.SetCursorPosition(20, 22);
            Console.Write("▄ Login: " + "{0,32}\n", a);
            Console.SetCursorPosition(20, 23);
            Console.Write("▄ Password: " + "{0,29}\n", a);
            Console.SetCursorPosition(20, 24);
            Console.Write("▄ Confirm password: " + "{0,21}\n", a);
            Console.SetCursorPosition(20, 25);
            Console.Write("▄ FirstName: " + "{0,28}\n", a);
            Console.SetCursorPosition(20, 26);
            Console.Write("▄ LastName: " + "{0,29}\n", a);
            Console.SetCursorPosition(20, 27);
            Console.Write("▄ Phone: " + "{0,32}\n", a);
            Console.SetCursorPosition(20, 28);
            Console.Write("▄ Email: " + "{0,32}\n", a);
            Console.SetCursorPosition(20, 29);
            Console.WriteLine("▀▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▀");
            Console.ForegroundColor = ConsoleColor.Black;
            Console.SetCursorPosition(29, 22);
            LogPas[0] = Console.ReadLine();
            bool s = true;
            while (s)
            {
                Console.SetCursorPosition(31, 23);
                LogPas[1] = Console.ReadLine();
                Console.SetCursorPosition(39, 24);
                LogPas[6] = Console.ReadLine();
                if ((LogPas[1] != "") && (LogPas[6] != "") && (LogPas[1].Equals(LogPas[6])))
                {
                    s = false;
                }
                else
                {
                    Console.SetCursorPosition(31, 23);
                    Console.Write("               ");
                    Console.SetCursorPosition(39, 24);
                    Console.Write("               ");
                }
            }
            Console.SetCursorPosition(32, 25);
            LogPas[2] = Console.ReadLine();
            Console.SetCursorPosition(32, 26);
            LogPas[3] = Console.ReadLine();
            Console.SetCursorPosition(29, 27);
            LogPas[4] = Console.ReadLine();
            Console.SetCursorPosition(29, 28);
            LogPas[5] = Console.ReadLine();

            Console.BackgroundColor = ConsoleColor.Gray;
            Console.ForegroundColor = ConsoleColor.White;

            Console.Clear();
            return LogPas;
            
        }

        public static void SumPrint(double s)
        {
            Console.WriteLine("Your Sum: {0,0:C}");
        }
        public static void View(string[] Data)
        {
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("{0,5} {1,13} {2,8} {3,13} {4,20} \n", Data);
        }

        public static void ViewUsers(string[] Data)
        {
            Console.ForegroundColor = ConsoleColor.DarkGreen;
            string[] headerColumns = new string[] { "FirstName   ", "LastName    ", "Login   ", "PhoneNumber", "Mail" };
            Console.WriteLine("{0,5} {1,5} {2,5} {3,10} {4,15}\n", headerColumns);
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("{0,10} {1,10} {2,10} {4,15} {3,17}\n", Data);
        }

        public static void Clear()
        {
            Console.Clear();
        }
        public static void AddRoom()
        {
            Console.WriteLine("Add room in base");
            }
        public static string AddRoomNumber()
        {
            Console.WriteLine("Enter number Room");
            return Console.ReadLine();
        }
        public static byte AddRoomNumberPlaces()
        {
            Console.WriteLine("Enter number places");
            return Convert.ToByte(Console.ReadLine());
        }
        public static double AddRoomPrice()
        {
            Console.WriteLine("Enter price of Room");
            return Convert.ToDouble(Console.ReadLine());
        }
        public static string AddRoomDescription()
        {
            Console.WriteLine("Enter description of Room");
            return Console.ReadLine();
        }
        public static string AddRoomType()
        {
            Console.WriteLine("Choose type of Room: ");
            Console.WriteLine("1) Standart, 2)Studio, 3)Business, 4)Duplex, 5)Apartment, 6)President");
            return Console.ReadLine();
        }

        public static void RightEnter()
        {
            Console.WriteLine("Please correct enter");
        }
        public static string RemoveRoom()
        {
            Console.WriteLine("Enter number room");
            return Console.ReadLine();
        }

        public static bool SearchByBook()
        {
            Console.WriteLine( "Seacrh by booking. Choose please 1)Booking: Other)Not booking" );
            string key = Console.ReadLine();
            if (key=="1")
                return true;
            else return false;
        }

        public static DateTime DateStart()
        {
            Console.SetCursorPosition(0,10);
            DateTime Date = DateTime.Now.Date;
            string[] s = new string[] { Convert.ToString(Date.Year), Convert.ToString(Date.Month) };
            myCalendar calendar = new myCalendar();
            calendar.PrintCalendar(s);
            Console.WriteLine("Enter start booking date");
            DateTime date = DataReadConsole.EnterDat();
            return date;
        }
        public static DateTime DateEnd()
        {
            Console.SetCursorPosition(0, 10);
            DateTime Date = DateTime.Now.Date;
            string[] s = new string[] { Convert.ToString(Date.Year), Convert.ToString(Date.Month) };
            myCalendar calendar = new myCalendar();
            calendar.PrintCalendar(s);
            Console.WriteLine("Enter end booking date");
            DateTime date = DataReadConsole.EnterDat();
            return date;
        }

        public static string Seacrh()
        {
            Console.WriteLine(" Choose parametrs for search");
            Console.WriteLine("1-ByBook; 2-ByNumber; 3-ByTypeRoom");
            return Console.ReadLine();
        }
        public static string SortBy()
        {
            Console.WriteLine(" Choose parametrs for sort");
            Console.WriteLine("1-ByDirection; 2-ByNumber; 3-ByTypeRoom;");
            return Console.ReadLine();
        }
        public static string SortPrice()
        {
            Console.WriteLine(" Choose parametrs for sort");
            Console.WriteLine("1-Up price; 2-Down price;");
            return Console.ReadLine();
        }

        public static string SearchByNumber()
        {
            Console.WriteLine("Enter number room");
            return Console.ReadLine();
        }

        public static void Print(string s)
        {
            Console.WriteLine(s);
        }

        public static void Print(object s)
        {
            Console.WriteLine(s);
        }

        public static string ToBooking()
        {
            Console.WriteLine( "Enter please number room for booking" );
            return Console.ReadLine();
        }

    }
}
