﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using DataModelLib;

namespace ClientInterface
{
    public class MenuwithPodmenu
    {
        public class ConsolePodMenu
        {
            public int a { get; set; }

            public delegate void MenuMethod();

            public static List<MenuMethod> Methods;

            public ConsolePodMenu(params MenuMethod[] methods)
            {
                Methods = new List<MenuMethod>();
                Methods.AddRange(methods);
            }

            public static int SelectedItem { get; private set; }
            public static int right; //Положение первой строки меню
            public static int top;



            public void Show(int a, bool addEmptyLineBefore = true)
            {
                int right = 0;//Положение первой строки меню
                //if (addEmptyLineBefore)
                //{
                //    Console.WriteLine();
                //    right++;
                //}

                //top = Console.CursorTop;
                //if (addEmptyLineBefore)
                //{
                //    Console.WriteLine();
                //    top++;
                //}

                Console.SetCursorPosition(a, 1);
                Console.BackgroundColor = ConsoleColor.DarkCyan;
                Console.Write("┌───────────┐");
                
                for (int i = 0; i < Methods.Count; i++)
                {
                    if (i == SelectedItem)
                    {
                        Console.BackgroundColor = ConsoleColor.DarkGray;
                        Console.ForegroundColor = ConsoleColor.DarkRed;
                    }
                    else
                    {
                        Console.BackgroundColor = ConsoleColor.DarkCyan;
                        Console.ForegroundColor = ConsoleColor.Black;
                    }
                    Console.ForegroundColor = ConsoleColor.White;
                    Console.SetCursorPosition(a, i+2);
                    Console.Write("{1,0} {0,9} {1,0}", Methods[i].Method.Name, "│");
                }
                Console.SetCursorPosition(a, Methods.Count + 2);
                Console.BackgroundColor = ConsoleColor.DarkCyan;
                Console.Write("└───────────┘");
                Console.BackgroundColor = ConsoleColor.Gray;
                Console.WriteLine();
                WaitForInput();
                Console.Clear();
                Console.ForegroundColor = ConsoleColor.Black;
                Console.BackgroundColor = ConsoleColor.Gray;
            }


            internal void WaitForInput()
            {
                up:
                ConsoleKeyInfo cki = Console.ReadKey();
                switch (cki.Key)
                {
                    case ConsoleKey.RightArrow:
                        MoveRight();
                        break;
                    case ConsoleKey.LeftArrow:
                        MoveLeft();
                        break;
                    case ConsoleKey.UpArrow:
                        break;
                    case ConsoleKey.DownArrow:
                        goto up;
                    case ConsoleKey.Enter:
                        Methods[SelectedItem]();
                        Show(a);
                        break;
                }
            }

            private void MoveRight()
            {
                SelectedItem = SelectedItem == Methods.Count - 1 ? 0 : SelectedItem + 1;
                Console.SetCursorPosition(0, right);
                Show(a, false);
            }

            //private void MoveDown()
            //{
            //    SelectedItem = SelectedItem == Methods.Count - 1 ? 0 : SelectedItem + 1;
            //    Console.SetCursorPosition(a, top);

            //    Show(false);
            //}

            private void MoveLeft()
            {
                SelectedItem = SelectedItem == 0 ? Methods.Count - 1 : SelectedItem - 1;
                Console.SetCursorPosition(0, right);
                Show(a, false);
            }
        }

        public class ConsoleMenu
        {

            public delegate void MenuMethod();

            public static List<MenuMethod> Methods;

            public ConsoleMenu(params MenuMethod[] methods)
            {
                Methods = new List<MenuMethod>();
                Methods.AddRange(methods);
            }

            public ConsoleColor ItemColor;
            public ConsoleColor SelectionColor;
            public static int SelectedItem { get; private set; }
            public static int right; //Положение первой строки меню

            public void Show(bool addEmptyLineBefore = true)
            {
                //int right; //Положение первой строки меню
                //right = Console.CursorLeft;
                //if (addEmptyLineBefore)
                //{
                //    Console.WriteLine();
                //    right++;
                //}

                //top = Console.CursorTop;
                //if (addEmptyLineBefore)
                //{
                //    Console.WriteLine();
                //    top++;
                //}
                Console.BackgroundColor = ConsoleColor.Gray;
                Console.ForegroundColor = ConsoleColor.White;
                Console.Clear();
                Console.SetCursorPosition(0, 0);
                for (int i = 0; i < Methods.Count; i++)
                {
                    if (i == SelectedItem)
                    {
                        Console.BackgroundColor = ConsoleColor.DarkGray;
                        Console.ForegroundColor = ConsoleColor.DarkRed;
                        Console.Write(" | " + "M");
                    }
                    else
                    {
                        Console.BackgroundColor = ConsoleColor.DarkCyan;
                        Console.ForegroundColor = ConsoleColor.Black;
                        Console.Write(" | " + "M");

                    }
                    Console.ForegroundColor = ConsoleColor.White;
                    Console.Write(Methods[i].Method.Name + " | ");
                }
                Console.BackgroundColor = ConsoleColor.Gray;
                Console.WriteLine();
                //Console.WriteLine("Нажмите любую клавишу для выхода...");
                WaitForInput();
                Console.ForegroundColor = ConsoleColor.Black;
                Console.BackgroundColor = ConsoleColor.Gray;
                Console.Clear(); 
            }


            internal void WaitForInput()
            {
                Up:
                ConsoleKeyInfo cki = Console.ReadKey();
                switch (cki.Key)
                {
                    case ConsoleKey.RightArrow:
                        MoveRight();
                        break;
                    case ConsoleKey.LeftArrow:
                        MoveLeft();
                        break;
                    case ConsoleKey.UpArrow:
                        goto Up;
                    case ConsoleKey.DownArrow:
                        goto Up;
                    case ConsoleKey.Enter:
                        Methods[SelectedItem]();
                        Show();
                        break;
                    case ConsoleKey.Escape:
                        Environment.Exit(0);
                        break;
                    default:
                        goto Up;
                }

            }
            private void MoveRight()
            {
                SelectedItem = SelectedItem == Methods.Count - 1 ? 0 : SelectedItem + 1;
                Console.SetCursorPosition(0, right);
                Show(false);
            }
            private void MoveLeft()
            {
                SelectedItem = SelectedItem == 0 ? Methods.Count - 1 : SelectedItem - 1;
                Console.SetCursorPosition(0, right);
                Show(false);
            }
        }
    }

    
}