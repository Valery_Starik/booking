﻿using System;
using System.Text.RegularExpressions;

namespace ClientInterface
{
    public static class DataReadConsole
    {
        public static bool Regular(string pattern, string text)
            {
                MatchCollection Matches = Regex.Matches(text, pattern, RegexOptions.IgnoreCase | RegexOptions.ExplicitCapture);
                string res = "";
                foreach (Match nextMatch in Matches)
                {
                    res = nextMatch.ToString();

                }
                if (res != "")
                    return true;
                return false;
            }

        public static DateTime EnterDat()
        {
            DateTime dt = new DateTime(); DateTime time;
            bool b = true;
            while (b)
            { 
                Console.WriteLine("Insert date: dd,MM, YY");
                if (!DateTime.TryParse(Console.ReadLine(), out dt))
                {
                    Console.WriteLine("Not valid date format!");
                    b = true;
                }
                Console.WriteLine("Insert time: hh:mm:ss");
                if (!DateTime.TryParse(Console.ReadLine(), out time))
                {
                    Console.WriteLine("Not valid time format!");
                    b = true;
                }
                if (dt > DateTime.Now.Date)
                {b = false;}
                if (dt <= DateTime.Now.Date)
                {Console.WriteLine("This date is old!"); b = true;}

            }
            Console.Clear();
            return dt;
        }

        }

    }
