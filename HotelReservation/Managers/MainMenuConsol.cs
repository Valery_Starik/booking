﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ClientInterface;
using DataModelLib;

namespace Managers
{
    public class MainMenuConsol
        {

            private static UserDispatcher userDispatcher = new UserDispatcher();

            public static void StartAdminMenu()
            {
            userDispatcher.eventadmin += Events.EventKeyPressAdmin_Handler;
            MenuwithPodmenu.ConsoleMenu consoleMenuAdmin = new MenuwithPodmenu.ConsoleMenu(Files, Edit, SortBy, Find, Views, Exit);
            consoleMenuAdmin.Show();
            }

        public static void StartUserMenu()
        {
            userDispatcher.eventadmin += Events.EventKeyPressAdmin_Handler;
            MenuwithPodmenu.ConsoleMenu consoleMenuUser = new MenuwithPodmenu.ConsoleMenu(Files, ToBook, SortBy, Find, View, Exit);
            consoleMenuUser.Show();
        }

        private static void Views()
        {
            MenuwithPodmenu.ConsolePodMenu consolePodMenuViews =
                new MenuwithPodmenu.ConsolePodMenu(Booked, Users, Manager.Viewer);
            consolePodMenuViews.Show(48);
        }

        private static void Users()
        {
            Manager.ViewUsers();
        }

        private static void Booked()
        {
            Manager.FindBook();
        }

        private static void ToBook()
        {
            MenuwithPodmenu.ConsolePodMenu consolePodMenuBook =
                new MenuwithPodmenu.ConsolePodMenu(Manager.ToBook, Manager.UnBook);
            consolePodMenuBook.Show(12);
        }

        private static void View()
        {
            MenuwithPodmenu.ConsolePodMenu consolePodMenuView =
                new MenuwithPodmenu.ConsolePodMenu(Manager.ToBook, Manager.UnBook);
            consolePodMenuView.Show(48);
        }

        private static void Files()
        {
            MenuwithPodmenu.ConsolePodMenu consolePodMenuFile =
                new MenuwithPodmenu.ConsolePodMenu(Load, Save, Manager.LogOut, Help, Exit);
            consolePodMenuFile.Show(0);
        }
        private static void Edit()
        {
            MenuwithPodmenu.ConsolePodMenu consolePodMenuEdit =
            new MenuwithPodmenu.ConsolePodMenu(Manager.AddRoom, Manager.Delete, Manager.Clear);
            consolePodMenuEdit.Show(12);
        }
        private static void SortBy()
        {
            MenuwithPodmenu.ConsolePodMenu consolePodMenuSortBy =
                new MenuwithPodmenu.ConsolePodMenu(Manager.SortPrice, Manager.SortNumb, Manager.SortType);
            consolePodMenuSortBy.Show(24);
        }
        private static void Find()
        {
            MenuwithPodmenu.ConsolePodMenu consolePodMenuFind =
                new MenuwithPodmenu.ConsolePodMenu(Manager.FindBook, Manager.FindNumb, Manager.FindType);
            consolePodMenuFind.Show(36);
        }

        private static void Exit(){Environment.Exit(0);}
        
            private static void Save(){Manager.SaveHotel(Hotel);}

            public static Hotel Hotel { get; set; }

            private static void Load(){Manager.LoadHotel();}

           public static void Help()
            {
                //ПОТОМ НАПИШУ ЧТО-НИБУДЬ
            }
        }

    }

