﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Design;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataModelLib;
using System.ComponentModel;

namespace Managers
{
    public delegate void ChooseUsers(List<User> users, string UserLogIn, string Enter, Hotel OurHotel);
    public delegate void PressKeyMenu(List<User> users, string UserLogIn, string Enter, Hotel OurHotel);
    public delegate void MenuAdmin(string method);

    public class UserDispatcher
    {
        public event ChooseUsers eventUser = null;
        public event PressKeyMenu eventPressKey = null;
        public event MenuAdmin eventadmin = null;
        public void EventDispatcher(List<User> users, string UserLogIn, string Enter, Hotel OurHotel)
        {
            if (eventUser != null)
            {
                eventUser.Invoke(users, UserLogIn, Enter, OurHotel);
            }
        }
       

        public void EventKeyDispatcher(List<User> users, string UserLogIn, string Enter, Hotel OurHotel)
        {
            if (eventPressKey != null)
            {
                eventPressKey.Invoke(users, UserLogIn, Enter, OurHotel);
            }
        }
        public void EventKeyDispatcher(string method)
        {
            if (eventadmin != null)
            {
                eventadmin.Invoke(method);
            }
        }
    }

    public class Events
    {
        public static void EventUsers_Handler(List<User> users, string UserLogIn, string EnterUser, Hotel OurHotel)
        {
            if (EnterUser == "Admin")
            {
                MainMenuConsol.StartAdminMenu();
            }
            else
            {
                MainMenuConsol.StartUserMenu();
            }
        }

        public delegate void Dict();
        private static Dictionary<string, Dict> _method;

        //For new menu
        public static void EventKeyPressAdmin_Handler(string method)
        {
            ///DICTIONARY/////
            _method = new Dictionary<string, Dict>
                {
                    { "AddRoom", Manager.AddRoom},
                    { "Delete", Manager.Delete},
                    { "SortPrice", Manager.SortPrice},
                    { "SortNumb", Manager.SortNumb},
                    { "SortType", Manager.SortType},
                    { "ToBook", Manager.ToBook},
                    { "Clear", Manager.Clear},
                    { "FindBook", Manager.FindBook},
                    { "FindNumb", Manager.FindNumb},
                    { "FindType", Manager.FindType}
                    
                };
            _method[method]();
        }
    }
}
