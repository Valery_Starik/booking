﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataModelLib;
using ClientInterface;

namespace Managers
{
    [Serializable]
    public static class Manager
    {
        public static User UserLogIn = null;
        private static UserCollection userCollection = new UserCollection();
        private static List<User> users = new List<User>();
        private static UserDispatcher userDispatcher = new UserDispatcher();
        private static Hotel OurHotel = new Hotel();

        //LogIn users or create and logIn users
        public static void LogOut()
        {
            UserLogIn = null;
            userDispatcher.eventUser += Events.EventUsers_Handler;
            string[] s = ClientConsole.User();
            string EnterUser = s[0];
            int index = userCollection.Searching(EnterUser, users);

            if (index == -1)
            {
                string [] ss = new string[7];
                ss=ClientConsole.CreateUser();
                string data = ss[2] + " " + ss[3] + " " + ss[4] + " " + ss[0] + " " + ss[1] + " " + ss[5] ;
               userCollection.Addition(data, users);
                index = userCollection.Searching(EnterUser, users);
                UserLogIn = users[index];
                Serialization.WriteXMLSerial(users);
            }
            else
            {
                UserLogIn = users[index];
            }
            userDispatcher.EventDispatcher(users, UserLogIn.ToString(), EnterUser, OurHotel);
            userDispatcher.eventUser -= Events.EventUsers_Handler;
        }

        public static void SaveHotel(Hotel OurHotel)
        {
            Serialization.WriteXMLSerial(OurHotel);
        }

        public static Hotel LoadHotel()
        {
            Hotel OurHotel = Serialization.ReadXMLSerialHotel();
            return OurHotel;
        }

        public static void FindBook()
        {
            List<HotelRoom> p = new List<HotelRoom>();
            p = OurHotel.SearchByBooked(ClientConsole.SearchByBook(),ClientConsole.DateStart(),ClientConsole.DateEnd());
            Viewer(p);
        }

        public static void ViewUsers()
        {
            foreach (User p in users)
            {
                string[] Data = { p.FirstName, p.LastName, p.Login, p.Mail, p.PhoneNumber };
                ClientConsole.ViewUsers(Data);
            }
        }

        public static void FindNumb()
        {
            Hotel ourHotel = new Hotel();
            ourHotel = new Hotel { OurHotel.SearchByNumber(Convert.ToString(ClientConsole.SearchByNumber())) };
            Viewer(ourHotel);
        }

        public static void FindType()
        {
            List<HotelRoom> p = new List<HotelRoom>();
            p = OurHotel.SearchByType(TRooms(ClientConsole.AddRoomType()));
            Viewer(p);
        }

        private static void Viewer(List<HotelRoom> ourHotel)
        {
            ClientConsole.ConsoleViewHeader();
            foreach (HotelRoom p in ourHotel)
            {
                string[] Data = { p.NumberRoom, p.TypeRoom.ToString(), Convert.ToString(p.Price),
                    p.IsBooked(DateTime.Now, DateTime.Now.AddDays(1)) ? "Yes" : "No" , p.Description};
                ClientConsole.View(Data);
            }
        }

        public static void Clear()
        {
            OurHotel.Clear();
        }

        public static TypeRooms TRooms(string type)
        {
            TypeRooms ttRotypeRoom = (TypeRooms) Convert.ToInt16(type);
            return ttRotypeRoom;
        }

    public static void AddRoom()
        {
            string NumberRoom = ClientConsole.AddRoomNumber();
            double Price = ClientConsole.AddRoomPrice();
            string Description = ClientConsole.AddRoomDescription();
            string type = ClientConsole.AddRoomType();
            byte numberPlaces = ClientConsole.AddRoomNumberPlaces();
            OurHotel.Add(NumberRoom, TRooms(type), Price, Description, numberPlaces);
            Serialization.WriteXMLSerial(OurHotel);
            }

        public static void Viewer()
        {
            ClientConsole.ConsoleViewHeader();
            foreach (HotelRoom p in OurHotel)
            {
                string[] Data = { p.NumberRoom, p.TypeRoom.ToString(), Convert.ToString(p.Price),
                    p.IsBooked(DateTime.Now, DateTime.Now.AddDays(1)) ? "Yes" : "No", p.Description};
                ClientConsole.View(Data);
            }
        }
        public static void Viewer(Hotel ourHotel)
        {
            ClientConsole.ConsoleViewHeader();
            foreach (HotelRoom p in ourHotel)
            {
                string[] Data = { p.NumberRoom, p.TypeRoom.ToString(), Convert.ToString(p.Price),
                    p.IsBooked(DateTime.Now, DateTime.Now.AddDays(1)) ? "Yes" : "No", p.Description};
                ClientConsole.View(Data);
            }
        }

        public static void ToBook()
        {
            HotelRoom room = OurHotel.SearchByNumber(ClientConsole.ToBooking());
            DateTime start = ClientConsole.DateStart();
            DateTime end = ClientConsole.DateEnd();
            TimeSpan Price = end - start;
            double Sum = Price.Days*room.Price; 
            ClientConsole.SumPrint(Sum);   
            OurHotel.BookRoom(room, UserLogIn, start, end);
            string email = UserLogIn.Mail;
            Mail mail = new Mail();
            mail.SendMail(email, "Your room is booking", "Your room is booking");
        }
        public static void UnBook()
        {
            HotelRoom room = OurHotel.SearchByNumber(Convert.ToString(ClientConsole.ToBooking()));
            OurHotel.UnBookRoom(room, UserLogIn);
        }
        public static void SortPrice()
        {
            string ch = ClientConsole.SortPrice();
            switch (ch)
            {
                case "1":
                    OurHotel.SortByPrice(ListSortDirection.Descending);
                    break;
                default:
                    OurHotel.SortByPrice(ListSortDirection.Ascending);
                    break;
            }
        }

        public static void SortNumb()
        {
            OurHotel.SortByNumberRoom(ListSortDirection.Ascending);
        }

        public static void SortType()
        {
            OurHotel.SortByType(ListSortDirection.Ascending);
        }
        
        public static void Delete()
        {
            OurHotel.RemoveRoom(ClientConsole.RemoveRoom());
        }
        public static void Start()
        {
            ClientConsole.StartConsol();
            users = Serialization.ReadXMLSerial();
            OurHotel = Serialization.ReadXMLSerialHotel();
            LogOut();
        }

    }
}
