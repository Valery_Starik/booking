﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using DataModelLib;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using Assert = NUnit.Framework.Assert;


namespace DataModelLib.Tests
{
    [TestFixture]
    public class SerializationTests
    {
        [SetUp]
        public static void SetUpTestsMethod()
        {
            
        }

        [Test]
        public void SaveTest()
        {
            User a = new User();
            a.FirstName = "aa";
            Serialization.Save(a,"SaveTest.dat");
            User b = new User();
            b=(User) Serialization.Load("SaveTest.dat");
            Assert.AreEqual(b.FirstName, a.FirstName);
        }

        [Test]
        public void LoadTest()
        {
            User b = new User();
            b = (User)Serialization.Load("SaveTest.dat");
            Assert.AreEqual("aa",b.FirstName);
        }

        [Test]
        public void WriteXMLSerialTest()
        {
           HotelRoom a = new HotelRoom();
            a.NumberRoom = "1";
            a.Price = 32;
            a.TypeRoom = TypeRooms.Apartment;
            Hotel b = new Hotel();
            b.Add(a);
            Serialization.WriteXMLSerial(b);
            Hotel tt = new Hotel();
            tt = Serialization.ReadXMLSerialHotel();
            Assert.AreEqual(tt, b);
        }

        [Test]
        public void ReadXMLSerialTest()
        {
            List<User> a = new List<User>();
            User b = new User();
            b.FirstName = "Kolya";
            a.Add(b);
            List<User> c = new List<User>();
            Serialization.WriteXMLSerial(a);
            c = Serialization.ReadXMLSerial();
            Assert.AreEqual(Convert.ToString(a), Convert.ToString(c));
        }

        [Test]
        public void WriteXMLSerialTest1()
        {
            List<User> a = new List<User>();
            User b = new User();
            b.FirstName = "Kolya";
            a.Add(b);
            List<User> c = new List<User>();
            Serialization.WriteXMLSerial(a);
            c = Serialization.ReadXMLSerial();
            Assert.AreEqual(Convert.ToString(c), Convert.ToString(a));
        }

        [Test]
        public void ReadXMLSerialHotelTest()
        {
            HotelRoom a = new HotelRoom();
            a.NumberRoom = "1";
            a.Price = 32;
            a.TypeRoom = TypeRooms.Apartment;
            Hotel b = new Hotel();
            b.Add(a);
            Serialization.WriteXMLSerial(b);
            Hotel tt = new Hotel();
            tt = Serialization.ReadXMLSerialHotel();
            Assert.AreEqual(Convert.ToString(b), Convert.ToString(tt));
        }
    }
}